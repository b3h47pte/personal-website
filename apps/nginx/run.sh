#!/bin/bash
set -xe

# Obtain Let's Encrypt certificate at first w/o nginx since nginx won't start
# without the SSL certificate.
certbot -n --nginx -d mikehbao.com -d www.mikehbao.com --agree-tos --email me@mikehbao.com --redirect

nginx -s stop
service cron start
nginx -g 'daemon off;'
