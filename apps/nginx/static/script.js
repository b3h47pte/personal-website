// Change 'job' text interval
jobTextList = [
    "PHD CANDIDATE",
    "R&D ENGINEER",
    "RESEARCHER",
    "SOFTWARE ENGINEER",
];
jobIndex = -1;
changeJobText = function() {
    jobIndex = (jobIndex + 1) % jobTextList.length;
    $("#job-text").slideUp(500, function(){
        $("#job-text").html(jobTextList[jobIndex]);
        $("#job-text").slideDown(500);
    });
}

changeJobText();
setInterval(changeJobText, 3000);

$("#mailto-link").on({
    mouseenter: function () {
        $("#mailto-link").attr("href", "mailto:mike.h.bao@gmail.com");
    },
    mouseleave: function() {
        $("#mailto-link").attr("href", "");
    }
});
