from setuptools import setup, find_packages
setup(
    name="website",
    version="0.1",
    package_dir={'':'src'},
    packages=find_packages('src'),
    entry_points={
        'paste.app_factory' : [
            'main = website:main',
        ]
    },
    package_data={
        '': ['*.jinja2']
    }
)
