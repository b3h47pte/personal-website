#
# Copyright 2019.
#
import pyramid

@pyramid.view.view_config(
    route_name='homePage',
    renderer='templates/homePage.jinja2')
def homePage(request):
    return dict()
