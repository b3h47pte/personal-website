#
# Copyright 2019.
#
from pyramid.config import Configurator

def main(globalConfig, **settings):
    with Configurator(settings=settings) as config:
        config.include('.routes')
        config.include('pyramid_jinja2')
        config.scan()
        return config.make_wsgi_app()
