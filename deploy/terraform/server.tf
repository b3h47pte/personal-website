provider "google" {
    credentials     = "/etc/gce-terraform.json"

    project         = "personal-website-239605"
    region          = "us-central1"
    zone            = "us-central1-c"
}

resource "google_compute_firewall" "default" {
    name            = "server-firewall"
    network         = google_compute_network.default.name

    allow {
        protocol    = "tcp"
        ports       = ["22", "80", "443"]
    }
}

resource "google_compute_network" "default" {
    name            = "server-network"
}

resource "google_compute_instance" "server_instance" {
    name            = "server"
    machine_type    = "f1-micro"
    boot_disk {
        initialize_params {
            image   = "gce-uefi-images/cos-stable"
            size    = 20
            type    = "pd-standard"
        }
    }

    network_interface {
        network     = google_compute_network.default.name
        access_config {
            nat_ip  = "146.148.74.38"
        }
    }
}
